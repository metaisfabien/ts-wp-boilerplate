# Typescript boilerplate


## Features

- Monorepo with npm workspace
- Auto build dependancies with npm reference
- sourcemap support
- webpack build
- Lambda one file build

## Structure

- apps: Applications folder
  - lambda: Lambda example
- packages: dependancies folder
  - core: dependancie exemple

## Usage

### Insall the dependancies:

```bash
$ npm i
```

## Dev build

```bash
$ npm run build:dev
```