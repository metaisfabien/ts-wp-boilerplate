.PHONY=build

install:
	npm ci

build:
	npm run build:dev -w @test/lambda

clean:
	rm -rf ./node_modules
	rm -f ./package-lock.json
	rm -rf ./apps/lambda/node_modules
	rm -rf ./apps/lambda/dist
	rm -rf ./packages/core/dist

