const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = (env) => {
  const environment = env.NODE_ENV || "production";
  return {
    entry: "./src/index.ts",
    target: "node",
    devtool: "source-map",
    resolve: {
      extensions: [".js", ".ts"],
      plugins: [
        new TsconfigPathsPlugin({
          configFile: "tsconfig.json",
        }),
      ],
    },
    watch: false,
    mode: environment,
    module: {
      rules: [
        {
          test: /\.ts$/,
          exclude: /node_modules/,
          use: {
            loader: "ts-loader",
            options: {
              projectReferences: true,
            },
          },
        },

        {
          test: /\.js$/,
          enforce: 'pre',
          use: ['source-map-loader'],
        }, 
      ],
    },
    output: {
       path: `${process.cwd()}/build`,
      filename: "index.js",
      // clean: true,
      devtoolModuleFilenameTemplate: function (info) {
        return "/" + encodeURI(info.absoluteResourcePath);
      },
    },
  };
};
