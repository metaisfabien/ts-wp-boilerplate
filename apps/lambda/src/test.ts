import { TestLib } from "@test/core"

export class Test {
    constructor(private testLib = new TestLib()) {
    
    }
    public test() {
        this.testLib.test()
    }
}